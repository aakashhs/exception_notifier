Exception Notifier

A demo app to illustrate exception notifier gem's working with mail notifier in ruby on rails

rails(4.2.6)
exception_notifier(4.2)
Postgresql database(9.5)
An intentional error created to generate exception notifier with mail.

change the 'exception_recipients' in 'config/application.rb' attributes to your mail-id 
and change the username and password in 'config/environments/development.rb' and 'config/environments/production.rb'
reconfigure database.yml
create and migrate
and run


clone and test, happy coding
